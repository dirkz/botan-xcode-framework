//
//  Platform.hpp
//  MakeXcodeFramework
//
//  Created by Dirk Zimmermann on 20/4/24.
//

#ifndef Platform_hpp
#define Platform_hpp

#include <string>

enum class OS {
    MacOS,
    iOS
};

inline std::string to_string(OS os) {
    switch (os) {
        case OS::MacOS:
            return "macOS";
        case OS::iOS:
            return "iOS";
    }
}

inline std::string os_name(OS os) {
    switch (os) {
        case OS::MacOS:
            return "macos";
        case OS::iOS:
            return "ios";
    }
}

enum class Arch {
    Arm64
};

inline std::string to_string(Arch arch) {
    switch (arch) {
        case Arch::Arm64:
            return "arm64";
    }
}

inline std::string cpu_name(Arch arch) {
    switch (arch) {
        case Arch::Arm64:
            return "arm64";
    }
}

inline std::string arch_name(Arch arch) {
    switch (arch) {
        case Arch::Arm64:
            return "arm64";
    }
}

struct Platform {
    OS os;
    Arch arch;
};

inline std::string to_string(const Platform& platform) {
    return "Platform " + to_string(platform.os) + "/" + to_string(platform.arch);
}

inline std::string os_name(const Platform& platform) {
    return os_name(platform.os);
}

inline std::string cpu_name(const Platform& platform) {
    return cpu_name(platform.arch);
}

inline std::string arch_name(const Platform& platform) {
    return arch_name(platform.arch);
}

#endif /* Platform_hpp */
