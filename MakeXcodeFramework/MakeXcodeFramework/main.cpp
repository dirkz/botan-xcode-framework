//
//  main.cpp
//  MakeXcodeFramework
//
//  Created by Dirk Zimmermann on 20/4/24.
//

#include <iostream>
#include <vector>
#include <filesystem>
#include <cstdlib>
#include <thread>
#include <algorithm>

#include "Platform.hpp"

using namespace std;

string quote(const string& str) {
    return "\"" + str + "\"";
}

int main(int argc, const char * argv[]) {
    string path = std::filesystem::current_path();
    cout << "current_path(): " << path << "\n";
    cout << "\n";

    constexpr char env_base_dir[] = "BASE_DIR";

    char *p_base_dir = getenv(env_base_dir);
    if (!p_base_dir || strlen(p_base_dir) == 0) {
        cerr << "missing " << env_base_dir;
        exit(EXIT_FAILURE);
    }

    string base_dir{p_base_dir};
    if (!filesystem::is_directory(base_dir)) {
        cerr << "not a directory: " << base_dir;
        exit(EXIT_FAILURE);
    }

    cout << env_base_dir << " " << base_dir << "\n";
    cout << "\n";

    const unsigned processor_count = max(std::thread::hardware_concurrency(), 2u);
    cout << processor_count << " CPUs\n";

    string config_command = base_dir + "/botan/configure.py";

    string config_json = path + "/build/build_config.json";

    vector<Platform> platforms{{OS::MacOS, Arch::Arm64}, {OS::iOS, Arch::Arm64}};

    for (const Platform& platform : platforms) {
        cout << to_string(platform) << "\n\n";
        string prefix = base_dir + "/target/" + to_string(platform.os) + "/" + to_string(platform.arch);
        string command = config_command;
        command += " --prefix=" + quote(prefix);
        command += " --os=" + quote(os_name(platform));
        command += " --cpu=" + quote(cpu_name(platform));
        command += " --cc=clang";
        string arch_param = "--arch " + arch_name(platform);
        command += " --cc-abi-flags=" + quote(arch_param);
        int config_result = system(command.c_str());
        if (config_result != 0) {
            cerr << "error: " << config_result << "\n";
            cerr << "\t" << command << "\n";
            exit(EXIT_FAILURE);
        }
        if (!filesystem::is_regular_file(config_json)) {
            cerr << "expected a build_config.json file: " << config_json << "\n";
            exit(EXIT_FAILURE);
        }
    }

    return 0;
}
